﻿using System;
using System.Linq;
using Boa.Constrictor.Logging;
using Boa.Constrictor.Screenplay;
using Boa.Constrictor.WebDriver;
using BoDi;
using FluentAssertions;
using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;
using Specflow.DogApi.Screenplay.Framework.Screenplay.Actors;
using static Specflow.DogApi.Screenplay.Framework.WebDriver.BrowserContext;

namespace Specflow.DogApi.Screenplay.Framework.Bindings
{
    [Binding]
    public sealed class Hooks
    {
        private IActor Actor;
        private readonly IObjectContainer _objectContainer;
        private static ScenarioContext? _scenarioContext;
        private static FeatureContext? _featureContext;
        
        public Hooks(IObjectContainer objectContainer, ScenarioContext? scenarioContext, FeatureContext? featureContext)
        {
            _scenarioContext = scenarioContext;
            _featureContext = featureContext;
            _objectContainer = objectContainer;
        }
        
        #region hooks
        [Scope(Tag = "browser", Feature = "Specflow With Screenplay")]
        [BeforeScenario(Order = 1)]
        public void CreateActor()
        {
            Actor = Actors.WayneWebDriver;
            _scenarioContext.ScenarioContainer.RegisterInstanceAs(Actor);
        }

        [BeforeScenario(Order = 2)]
        public void AssignActorWebDriverAbility()
        {
            Actor = _scenarioContext.ScenarioContainer.Resolve<Actor>();

            var browserName = GetBrowserNameFromScenarioTag();

            if (!string.IsNullOrWhiteSpace(browserName))
            {
                switch (browserName.ToLower())
                {
                    case "bravebrowser":
                        Actor.Can(BrowseTheWeb.With(new ChromeDriver(BraveBrowserOptions())));
                        break;
                    case "chromebrowser":
                        Actor.Can(BrowseTheWeb.With(new ChromeDriver(ChromeOptions())));
                        break;
                }
            }

            _scenarioContext.ScenarioContainer.RegisterInstanceAs(Actor);
        }
        
        [AfterScenario]
        public void TeardownScenario()
        {
            // Call quit methods from container
            _scenarioContext.ScenarioContainer.Resolve<Actor>()
                .AttemptsTo(QuitWebDriver.ForBrowser());
            
            // FINAL BOSS for killing the *driver.exe process
            KillProcess("chromedriver.exe");
        }
        
        #endregion
       
        #region private helpers
        private string GetBrowserNameFromScenarioTag()
        {
            return GetTokenByScenarioTag("browser");
        }
        private string GetTokenByScenarioTag(string tagName)
        {
            string result = null;
            
            if (ScenarioTagIsPresent(tagName))
            {
                // Assert scenario has only a single 'browser' tag
                (GetScenarioTags() ?? Array.Empty<string>())
                    .Count(tag => tag.Contains(tagName)).Should().BeLessThan(2,
                        $"because there should only be a single @{tagName} tag per scenario.");

                return (GetScenarioTags() ?? Array.Empty<string>())
                    .FirstOrDefault(t => t.Contains(tagName, StringComparison.CurrentCultureIgnoreCase))!;
            }
            else
            {
                return result;
            }
        }
        private bool FeatureTagIsPresent(string tagName)
        {
            return GetFeatureTags()
                .Any(tag => tag.Contains(tagName, StringComparison.CurrentCultureIgnoreCase));
        }
        private bool ScenarioTagIsPresent(string tagName)
        {
            return (GetScenarioTags() ?? Array.Empty<string>())
                .Any(tag => tag.Contains(tagName, StringComparison.CurrentCultureIgnoreCase));
        }
        private string[] GetScenarioTags()
        {
            return _scenarioContext.ScenarioInfo.Tags ?? Array.Empty<string>();;
        }
        private string[] GetFeatureTags()
        {
            return _featureContext.FeatureInfo.Tags ?? Array.Empty<string>();
        }

        #endregion
    }
}