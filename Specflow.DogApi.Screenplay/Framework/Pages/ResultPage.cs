﻿using Boa.Constrictor.WebDriver;
using OpenQA.Selenium;
using static Boa.Constrictor.WebDriver.WebLocator;

namespace Specflow.DogApi.Screenplay.Framework.Pages
{
    public class ResultPage
    {
        public static IWebLocator ResultLinks => L(
            "DuckDuckGo Result Page Links",
            By.ClassName("result__a"));
    }
}