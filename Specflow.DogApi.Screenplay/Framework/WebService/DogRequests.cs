﻿using RestSharp;

namespace Specflow.DogApi.Screenplay.Framework.WebService
{
    public static class DogRequests
    {
        public static IRestRequest GetRandomDog() => new RestRequest("api/breeds/image/random", Method.GET);
    }
}