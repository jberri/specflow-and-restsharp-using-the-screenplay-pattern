﻿using System.Diagnostics;
using System.Linq;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;

namespace Specflow.DogApi.Screenplay.Framework.WebDriver
{
    public static class BrowserContext
    {
        public static ChromeOptions BraveBrowserOptions()
        {
            var options = new ChromeOptions();
            options.BinaryLocation = "C:\\Program Files\\BraveSoftware\\Brave-Browser\\Application\\brave.exe";
            options.AddArgument("--no-sandbox");
            options.AddArgument("--disable-gpu");
            options.AddArgument("--incognito");
            options.AddArgument("--start-maximized");
            options.AddArgument("--disable-extensions");
            
            return options;
        }
        public static ChromeOptions ChromeOptions()
        {
            var options = new ChromeOptions();
            options.AddArgument("--no-sandbox");
            options.AddArgument("--disable-gpu");
            options.AddArgument("--incognito");
            options.AddArgument("--start-maximized");
            options.AddArgument("--disable-extensions");
            
            return options;
        }

        public static void KillProcess(string processName)
        {
            Process.GetProcesses()
                .Where(p => string.CompareOrdinal(p.ProcessName, processName) == 0)
                .ToList()
                .ForEach(p => p.Kill());
        }
    }
}