﻿namespace Specflow.DogApi.Screenplay.Framework.Models
{
    public class DogResponse
    {
        public static string Message { get; set; }

        public static string Status { get; set; }
    }
}