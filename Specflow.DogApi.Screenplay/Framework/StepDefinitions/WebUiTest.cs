﻿using Boa.Constrictor.Screenplay;
using Boa.Constrictor.WebDriver;
using FluentAssertions;
using Specflow.DogApi.Screenplay.Framework.Pages;
using Specflow.DogApi.Screenplay.Framework.Screenplay.Interactions;
using TechTalk.SpecFlow;

namespace Specflow.DogApi.Screenplay.Framework.StepDefinitions
{
    [Binding]
    public sealed class WebUiTest
    {
        private readonly ScenarioContext _scenarioContext;
        private IActor Actor;

        public WebUiTest(ScenarioContext scenarioContext, Actor actor)
        {
            _scenarioContext = scenarioContext;
            Actor = actor;
        }


        [When(@"I navigate to the search page")]
        public void WhenINavigateToTheSearchPage()
        {
            Actor.AttemptsTo(Navigate.ToUrl(SearchPage.Url));
            Actor.AskingFor(ValueAttribute.Of(SearchPage.SearchInput)).Should().BeEmpty();
        }

        [When(@"submit a search for '(.*)'")]
        public void WhenSubmitASearchFor(string scenarioSearch)
        {
            // Use the scenario search string and provide a fallback
            string searchTerm = (!string.IsNullOrWhiteSpace(scenarioSearch)) ? scenarioSearch : "good boi!";

            Actor.AttemptsTo((Wait.Until(Appearance.Of(SearchPage.SearchInput), IsEqualTo.True())));
            Actor.AttemptsTo(SearchDuckDuckGo.For(searchTerm));
        }

        [Then(@"the search should be successful")]
        public void ThenTheSearchShouldBeSuccessful()
        {
            Actor.WaitsUntil(Appearance.Of(ResultPage.ResultLinks), IsEqualTo.True());
        }
    }
}