﻿using Boa.Constrictor.Logging;
using Boa.Constrictor.Screenplay;

namespace Specflow.DogApi.Screenplay.Framework.Screenplay.Actors
{
    public class Actors
    {
        public static Actor WayneWebDriver => new Actor("Wayne WebDriver", new ConsoleLogger());
        public static Actor AndrewApi => new Actor("Andrew Api", new ConsoleLogger());
        public static Actor SteveScreenplaySurfer => new Actor("Steve Screenplay Surfer", new ConsoleLogger());
    }
}