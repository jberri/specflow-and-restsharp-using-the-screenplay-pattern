﻿using System;
using System.IO;
using Boa.Constrictor.Screenplay;
using RestSharp;
using Specflow.DogApi.Screenplay.Framework.Models;
using Specflow.DogApi.Screenplay.Framework.WebService;
using DogsApi = Boa.Constrictor.RestSharp.Rest<Specflow.DogApi.Screenplay.Framework.Screenplay.Abilities.CallDogApi>;
using DogImagesApi = Boa.Constrictor.RestSharp.Rest<Specflow.DogApi.Screenplay.Framework.Screenplay.Abilities.CallDogImagesApi>;

namespace Specflow.DogApi.Screenplay.Framework.Screenplay.Interactions
{
    public class RandomDogImage : IQuestion<byte[]>
    {
        private RandomDogImage() {}
        
        public static RandomDogImage FromDogApi() => new RandomDogImage();

        public byte[] RequestAs(IActor actor)
        {
            // Make api call and store typed response
            var request = DogRequests.GetRandomDog();
            var response = actor.Calls(DogsApi.Request<DogResponse>(request));

            // Decompose response
            var resource = new Uri(DogResponse.Message).AbsolutePath;
            var imageRequest = new RestRequest(resource);
            var extension = Path.GetExtension(resource);

            // Make images api call
            var imageData = actor.Calls(DogImagesApi.Download(imageRequest, extension));

            return imageData;
        }
    }
}