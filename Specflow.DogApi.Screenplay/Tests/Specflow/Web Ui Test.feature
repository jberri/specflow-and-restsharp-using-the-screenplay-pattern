﻿@browser
Feature: Specflow With Screenplay
    In order to see good boi's in a webrowser
    As a test dev
    I want specflow features that leverage the screenplay pattern

@braveBrowser	
Scenario: Test DuckDuckGo Search
    When I navigate to the search page
    And submit a search for 'good doggo'
    Then the search should be successful