﻿using NUnit.Framework;
using Boa.Constrictor.Screenplay;
using Boa.Constrictor.WebDriver;
using OpenQA.Selenium.Chrome;
using Specflow.DogApi.Screenplay.Framework.Pages;
using FluentAssertions;
using Specflow.DogApi.Screenplay.Framework.Screenplay.Actors;
using Specflow.DogApi.Screenplay.Framework.Screenplay.Interactions;
using static Specflow.DogApi.Screenplay.Framework.WebDriver.BrowserContext;

namespace Specflow.DogApi.Screenplay.Tests.Screenplay
{
    [TestFixture]
    public class ScreenplayWebUiTest
    {
        private IActor Actor;
        
        [SetUp]
        public void InitialiseScreenplay()
        {
            Actor = Actors.SteveScreenplaySurfer;
            Actor.Can(BrowseTheWeb.With(new ChromeDriver(BraveBrowserOptions())));
        }
        
        [TearDown]
        public void QuitBrowser()
        {
            // Call screenplay-wrapped quit method
            Actor.AttemptsTo(QuitWebDriver.ForBrowser());
            
            // FINAL BOSS for killing *driver.exe process
            KillProcess("chromedriver.exe");
        }
        
        [Test]
        public void TestDuckDuckGoWebSearch()
        {
            // Act && Assert
            Actor.AttemptsTo(Navigate.ToUrl(SearchPage.Url));
            Actor.AskingFor(ValueAttribute.Of(SearchPage.SearchInput)).Should().BeEmpty();
            
            // Act && Assert
            Actor.AttemptsTo(SearchDuckDuckGo.For("good boi!"));
            Actor.WaitsUntil(Appearance.Of(ResultPage.ResultLinks), IsEqualTo.True());
        }
    }
}