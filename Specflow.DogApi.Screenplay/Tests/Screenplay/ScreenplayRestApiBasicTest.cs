﻿using NUnit.Framework;
using Boa.Constrictor.Screenplay;
using Boa.Constrictor.RestSharp;
using RestSharp;
using FluentAssertions;
using System.Net;
using Specflow.DogApi.Screenplay.Framework.Models;
using Specflow.DogApi.Screenplay.Framework.WebService;
using Specflow.DogApi.Screenplay.Framework.Screenplay.Actors;

namespace Specflow.DogApi.Screenplay.Tests.Screenplay
{
    [TestFixture]
    public class ScreenplayRestApiBasicTest
    {
        private IActor Actor;

        [SetUp]
        public void InitialiseScreenplay()
        {
            Actor = Actors.AndrewApi;
            Actor.Can(CallRestApi.Using(new RestClient("https://dog.ceo")));
        }

        [Test]
        public void TestDogApiStatusCode()
        {
            // Act
            var request = DogRequests.GetRandomDog();
            var response = Actor.Calls(Rest.Request(request));

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Test]
        public void TestDogApiContent()
        {
            // Act
            var request = DogRequests.GetRandomDog();
            var response = Actor.Calls(Rest.Request<DogResponse>(request));

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            DogResponse.Status.Should().Be("success");
            DogResponse.Message.Should().NotBeNullOrWhiteSpace();
        }
    }
}