﻿using NUnit.Framework;
using Boa.Constrictor.RestSharp;
using Boa.Constrictor.Screenplay;
using FluentAssertions;
using System.Net;
using Specflow.DogApi.Screenplay.Framework.Screenplay.Abilities;
using System.IO;
using System.Reflection;
using Specflow.DogApi.Screenplay.Framework.Screenplay.Interactions;
using Specflow.DogApi.Screenplay.Framework.Models;
using Specflow.DogApi.Screenplay.Framework.WebService;
using Specflow.DogApi.Screenplay.Framework.Screenplay.Actors;

namespace Specflow.DogApi.Screenplay.Tests.Screenplay
{
    [TestFixture]
    public class ScreenplayRestApiAdvancedTest
    {
        private IActor Actor;
        
        [SetUp]
        public void InitialiseScreenplay()
        {
            // Create Actor
            Actor = Actors.AndrewApi;
            var assemblyDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            
            // Assign Abilities
            Actor.Can(CallDogApi.DumpingTo(assemblyDir));
            Actor.Can(CallDogImagesApi.DumpingTo(assemblyDir));
        }

        [Test]
        public void TestDogApi()
        {
            // Act
            var request = DogRequests.GetRandomDog();
            var response = Actor.Calls(Rest<CallDogApi>.Request<DogResponse>(request));

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            DogResponse.Status.Should().Be("success");
            DogResponse.Message.Should().NotBeNullOrWhiteSpace();
        }

        [Test]
        public void TestDogApiImage()
        {
            // Act: call API and get dog image link
            var imageData = Actor.AsksFor(RandomDogImage.FromDogApi());
            
            // Assert for good boi!
            imageData.Should().NotBeNullOrEmpty();
        }
    }
}