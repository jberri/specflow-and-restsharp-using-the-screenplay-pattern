# Specflow and RestSharp Using the Screenplay Pattern

The Screenplay pattern is a new addition to the .NET test automation space.

This project is functionally identical to the control project [Specflow and RestSharp Using the Page Object Model Pattern](https://gitlab.com/jberri/specflow-and-restsharp-using-the-page-object-model-pattern). The only difference is that in _this_ project I use the Screenplay pattern to achieve the same ends. 


## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Setup](#setup)
* [Usage](#usage)
* [Acknowledgements](#acknowledgements)
* [Contact](#contact)


## General information
##### Q: What problem does it (intend to) solve?
**A:** Essentially to help me better understand what benefits or insights may be found in adopting the Screenplay pattern for test automation frameworks instead of opting for a traditional POM-based framework.

##### Q: What is the purpose of your project?
**A:** To serve as the comparison project against the control. I compare and explore both patterns in my [blog post here](https://jberri.gitlab.io/jgb/posts/2021-11-26-screenplay-vs-page-object-model/).

##### Q: Why did you undertake it?
**A:** Having written several POM frameworks from the ground-up I intentionally went in search of new ideas that might be taking test automation in new directions. I came across the blog of [Andrew Knight](https://automationpanda.com/) and his recent work on [open-sourcing a new .NET library](https://github.com/q2ebanking/boa-constrictor) he had developed based on the Screenplay pattern. While this pattern has existed in Java and Javascript for a while I believe Boa.Constrictor is the first C# implementation.


## Technologies Used
- **Project Type**: Class Library
- **.NET Framework**: NET6.0
- **Language**: c# v10
- **Key Libraries**:
    -   Specflow.Nunit
    -   Selenium.WebDriver.ChromeDriver
    -   RestSharp
    -   FluentAssertions
    -   Boa.Constrictor


## Features
- Implements a specflow-based browser test
- Implements a Screenplay-based browser test.
- Implements a basic API test against the [DogCeo api](https://dog.ceo/) using RestSharp.
- Implements a more advanced API test against the [DogCeo api](https://dog.ceo/) using RestSharp.


## Setup
I created this project in Windows 10 using JetBrains Rider. Tests are intended to run in Windows.


## Usage
Clone this repo and run in Windows using any IDE to build. The UI tests expect [BraveBrowser](https://brave.com/download/) to be installed, otherwise modify the code to use Chrome.

If you want to run this in Linux or MacOS you will need to download the appropriate platform [ChromeDriver binaries](https://chromedriver.chromium.org/downloads) and modify the code so it points to your binary.


## Acknowledgements
- This project was inspired by Andrew Knight's Screenplay library, [Boa Constrictor](https://github.com/q2ebanking/boa-constrictor).
- This project was based on [this tutorial](https://q2ebanking.github.io/boa-constrictor/tutorial/overview/).


## Contact
Created by [@jberri](https://jberri.gitlab.io/jgb/) - feel free to hmu.


## License 
This project is open source and available under the [MIT License](LICENSE).
